output:pngs
pngs: output.csv
	cd cardtex && make
output.csv: autocost/$(wildcard.cpp)
	cd autocost && make
rules:
	cd Rules && make
clean:
	cd cardtex && make clean
	cd autocost && make clean
	cd Rules && make clean
all:
	make rules
	make
