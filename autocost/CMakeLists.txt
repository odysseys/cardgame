﻿# CMakeList.txt : CMake project for autocost, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

project ("autocost")

# Add source to this project's executable.
add_executable (autocost "autocost.cpp" "autocost.h" "Card.cpp" "Card.h" "Cardcosts.cpp" "Cardcosts.h" "CardParser.cpp" "Cardparser.h" "CSVParser.cpp" "CSVParser.h" "Enums.h" "Output.cpp" "Output.h"   "CardBuilder.cpp" "CardBuilder.h")

# TODO: Add tests and install targets if needed.
