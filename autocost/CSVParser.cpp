#include "CSVParser.h"

CSVParser::CSVParser()
{
    lines = new string[1];

    cols = new string * [1];
    for (int j = 0; j < 1; j++)
        cols[j] = new string[1];
}

CSVParser::~CSVParser()
{
    csv.close();

    delete[] lines;
    for (int i = 0; i < count; i++)
        delete[] cols[i];
    delete[] cols;
}

void CSVParser::OpenFile(string _filename)
{
    count = 0;

    filename = _filename;
    if (filename == "")
    {
        cout << "No filename" << endl;
    }
    else
    {
        csv.open(filename, ios::in);

        if (!csv)
        {
            cout << "File not opened!" << endl;
        }
        else
        {
            cout << "File opened successfully!" << endl;
        }
    }
}

void CSVParser::ReadFile()
{
    if (csv.is_open())
    {
        while (!csv.eof())
        {
            csv.getline(section, 10000);
            count++;
            //cout << section;
            file += section;
            file += '|';
            file += '+';
            file += '|';
            file += '\n';
        }


        cout << '\n';
        count--;
        cout << count << endl;
        cout << file << endl;
    }
    else
    {
        cout << "File could not be read!" << endl;
    }
}

void CSVParser::CloseFile()
{
    file = "";
    colcount = 0;


    csv.close();
}

void CSVParser::SplitLines()
{
    lines = new string[count];

    if (lines == nullptr)
    {
        cout << "Lines pointer could not be allocated." << endl;
    }
    else
    {
        string tempfile = file;
        int found, i = 0;

        while (tempfile != "\0" && tempfile != "|+|\n")
        {
            found = tempfile.find('\n');
            for (int j = 0; j < found; j++)
            {
                lines[i] += tempfile[j];
            }

            tempfile.erase(0, found + 1);
            i++;
        }

        for (int k = 0; k < count; k++)
        {
            cout << lines[k] << endl;
        }

    }

    cout << "Line split complete." << endl;
}

void CSVParser::SplitCols()
{

    string tempfile = file;
    string templine;
    string tempsection;
    int found, found2, i = 0, i2 = 0;

    found = tempfile.find('\n');
    for (int j = 0; j < found; j++)
    {
        templine += tempfile[j];
    }

    while (templine != "\0")
    {
        found2 = templine.find('|');
        for (int j = 0; j < found2; j++)
        {
            tempsection += templine[j];
        }
        if (tempsection[1] == '+')
        {
            cout << tempsection;
            cout << colcount << endl;
            break;
        }

        templine.erase(0, found2 + 1);
        tempsection = "";
        colcount++;
    }

    cols = new string * [count];
    for (int j = 0; j < count; j++)
        cols[j] = new string[colcount];

    found = 0;
    found2 = 0;

    while (tempfile != "\0" && tempfile != "|+|\n")
    {
        found = tempfile.find('\n');
        for (int j = 0; j < found; j++)
        {
            templine += tempfile[j];
        }

        while (templine != "\0")
        {
            found2 = templine.find('|');
            for (int j = 0; j < found2; j++)
            {
                cols[i][i2] += templine[j];
            }
            templine.erase(0, found2 + 1);
            //cout << cols[i][i2] << endl;
            i2++;
        }

        tempfile.erase(0, found + 1);
        templine = "";
        i2 = 0;
        i++;
    }

    cout << "Column split complete." << endl;
}

void CSVParser::PrintLine(int _lineno)
{
    cout << lines[_lineno] << endl;
}

void CSVParser::PrintCol(int _lineno, int _colno)
{
    cout << cols[_lineno][_colno] << endl;
}

string CSVParser::GetCol(int _lineno, int _colno)
{
    string data;

    data = cols[_lineno][_colno];

    return data;
}
