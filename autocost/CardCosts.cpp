#include "CardCosts.h"

CardCosts::CardCosts()
{
    filename = "";
}

CardCosts::~CardCosts()
{

}

int CardCosts::CheckKeyword(string _value)
{
    string lower = "";

    for (int i = 0; i < _value.length(); i++)
    {
        lower += tolower(_value[i]);
    }
    
    _value = lower;

    if (_value == "flying")
    {
        return GetFlying();
    }
    else if (_value == "overrun")
    {
        return GetOverrun();
    }
    else if (_value == "bypass")
    {
        return GetBypass();
    }
    else if (_value == "resistance")
    {
        return GetResistance();
    }
    else if (_value == "inanimate")
    {
        return GetInanimate();
    }
    else if (_value == "determinded")
    {
        return GetDetermined();
    }
    else if (_value == "ranged")
    {
        return GetRanged();
    }
    else if (_value == "fury")
    {
        return GetFury();
    }
    else if (_value == "haste")
    {
        return GetHaste();
    }
    else if (_value == "extend")
    {
        return GetExtend();
    }
    else if (_value == "blocker")
    {
        return GetBlocker();
    }
    else if (_value == "unblockable")
    {
        return GetUnblockable();
    }
    return -1;
}

int CardCosts::CheckLocation(string _value, string _direction)
{
    string lower = "";

    for (int i = 0; i < _value.length(); i++)
    {
        lower += tolower(_value[i]);
    }

    _value = lower;

    if (_direction == "from")
    {
        if (_value == "battlefield")
        {
            return GetFromBattlefield();
        }
        else if (_value == "grave")
        {
            return GetFromGrave();
        }
        else if (_value == "hand")
        {
            return GetFromHand();
        }
        else if (_value == "deathzone")
        {
            return GetFromDeathzone();
        }
        else if (_value == "chronicle")
        {
            return GetFromChronicle();
        }
        else if (_value == "archieve")
        {
            return GetFromArchieve();
        }
    }
    else if (_direction == "to")
    {
        if (_value == "battlefield")
        {
            return GetToBattlefield();
        }
        else if (_value == "grave")
        {
            return GetToGrave();
        }
        else if (_value == "hand")
        {
            return GetToHand();
        }
        else if (_value == "deathzone")
        {
            return GetToDeathzone();
        }
        else if (_value == "conricletop")
        {
            return GetToChronicleTop();
        }
        else if (_value == "cronicle")
        {
            return GetToChronicle();
        }
    }
    return 0;
}

void CardCosts::LoadCosts()
{
    if (filename == "")
    {
        cout << "No cost filename" << endl;
    }
    else
    {
        costcsv.open(filename, ios::in);

        if (!costcsv)
        {
            cout << "Cost file not opened!" << endl;
        }
        else
        {
            cout << "Cost file opened successfully!" << endl;
        }
    }

    if (costcsv.is_open())
    {
        while (!costcsv.eof())
        {
            costcsv.getline(section, 10000);
            count++;
            //cout << section;
            file += section;
            file += '|';
            file += '\n';
        }


        cout << '\n';
        count--;
        cout << count << endl;
        cout << file << endl;
    }
    else
    {
        cout << "Cost file could not be read!" << endl;
    }
}

