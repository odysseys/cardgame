#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "Enums.h"

using namespace std;

class CardCosts
{
private:
	enum keywords
	{
		Flying,
		Overrun,
		Bypass,
		Resistance,
		Inanimate,
		Determinded,
		Ranged,
		Fury,
		Haste,
		Extend,
		Blocker,
		Unblockable
	};

	// Class variables
	string filename;
	fstream costcsv;
	string file;
	int count;
	char section[10000];

	// Card Types
	/* These variables are stuck in as initial values, to be replaced by the more
	up-to-date csv file values. */
	int unit = 0;
	int enhancement = 0;
	int conjured = 0;
	int trick = 30;
	int deathzonecost = 200;

	// Rarities
	int common = 0;
	int uncommon = 10;
	int rare = 30;
	int unique = 70;

	// Keywords
	int flying = 70;
	int overrun = 120;
	int resistance = 40;
	int bypass = 100;
	int inanimate = 50;
	int determined = 50;
	int ranged = 30;
	int fury = 100;
	int haste = 60;
	int extend = 2;
	int blocker = 20;
	int unblockable = 120;

	// Unit stats
	int attack = 40;
	int defence = 20;
	int boonattack = 40;
	int boondefence = 20;
	int tokenattack = 40;
	int tokendefence = 20;

	// Movement
	int bounce = 40;
	int search = 40;
	int rearrange = 60;
	int lookat = 70;
	int toreturn = 60;
	int destroy = 100;
	int reanimate = 120;
	int discardchoice = 80;
	int discardrandom = 120;
	int takecontrol = 110;
	int putintoplay = 140;
	int freeze = 30;

	// Exhaust
	int exhaust = 120;
	int preventready = 20;

	// Stand
	int stand = 80;

	// Repeatable effects
	int repeatonattack = 230;
	int repreatondraw = 200;
	int repeatonexhaust = 180;
	int repeatonexhaustany = 210;
	int repeatonpay = 0;
	int repeatonsac = 0;

	// Boon
	int genericresource = 80;
	int resource = 100;

	// Counter spell
	int countertype = 40;
	int counterany = 80;
	int counterability = 80;
	int counteranycard = 80;

	// Abilities
	int preventattack = 80;
	int preventblock = 110;
	int preventboth = 150;
	int forceattack = 100;
	int forceblock = 200;
	int swapstats = 60;
	int drawcard = 100;
	// maybe remove createtoken?
	int createtoken = 0;

	// Damage
	int damagetoany = 160;
	int damagetoplayer = 140;
	int damagetounit = 40;

	// From Location
	int frombattlefield = 20;
	int fromgrave = 30;
	int fromhand = 70;
	int fromdeathzone = 140;
	int fromcronicle = 10;
	int fromarchieve = 20;

	// To Location
	int tobattlefield = 60;
	int tograve = 60;
	int tohand = 60;
	int todeathzone = 140;
	int tochronicletop = 80;
	int tochronicle = 80;

	// Target
	int targetperm = 220;
	int taregtresource = 180;
	int targetunit = 20;
	int targetenhancement = 50;
	int targetplayer = 50;
	int targetyou = 30;
	int targetopponent = 50;
	int targeteveryone = 2;

	// Sacrifice
	int sacany = 130;
	int sactype = 180;

	// Reduce
	int reducegeneric = 30;
	int reduceresource = 60;

	// All
	int all = 3;

	// Ability cost reducion
	int abilityresource = 50;
	int abilitygeneric = 40;

	// For x values
	int Unitsinyourgraveyard = 2;

	// Temp
	int xspell = 100;
	int ueot = 10;

public:
	CardCosts();
	~CardCosts();

	// Setters for the local variables
	//{
	void SetUnit(int _x) { unit = _x; };
	void SetEnhancement(int _x) { enhancement = _x; };
	void SetConjured(int _x) { conjured = _x; };
	void SetTrick(int _x) { trick = _x; };
	void SetDeathzoneCost(int _x) { deathzonecost = _x; };
	void SetCommon(int _x) { common = _x; };
	void SetUncommon(int _x) { uncommon = _x; };
	void SetRare(int _x) { rare = _x; };
	void SetUnique(int _x) { unique = _x; };
	void SetFlying(int _x) { flying = _x; };
	void SetOverrun(int _x) { overrun = _x; };
	void SetResistance(int _x) { resistance = _x; };
	void SetBypass(int _x) { bypass = _x; };
	void SetInanimate(int _x) { inanimate = _x; };
	void SetDetermined(int _x) { determined = _x; };
	void SetRanged(int _x) { ranged = _x; };
	void SetFury(int _x) { fury = _x; };
	void SetHaste(int _x) { haste = _x; };
	void SetExtend(int _x) { extend = _x; };
	void SetBlocker(int _x) { blocker = _x; };
	void SetUnblockable(int _x) { unblockable = _x; };
	void SetAttack(int _x) { attack = _x; };
	void SetDefence(int _x) { defence = _x; };
	void SetBoonAttack(int _x) { boonattack = _x; };
	void SetBoonDefence(int _x) { boondefence = _x; };
	void SetTokenAttack(int _x) { tokenattack = _x; };
	void SetTokenDefence(int _x) { tokendefence = _x; };
	void SetBounce(int _x) { bounce = _x; };
	void SetSearch(int _x) { search = _x; };
	void SetRearrange(int _x) { rearrange = _x; };
	void SetLookAt(int _x) { lookat = _x; };
	void SetToReturn(int _x) { toreturn = _x; };
	void SetDestroy(int _x) { destroy = _x; };
	void SetReanimate(int _x) { reanimate = _x; };
	void SetDiscardChoice(int _x) { discardchoice = _x; };
	void SetDiscardRandom(int _x) { discardrandom = _x; };
	void SetTakeControl(int _x) { takecontrol = _x; };
	void SetPutIntoPlay(int _x) { putintoplay = _x; };
	void SetFreeze(int _x) { freeze = _x; };
	void SetExhaust(int _x) { exhaust = _x; };
	void SetPreventReady(int _x) { preventready = _x; };
	void SetStand(int _x) { stand = _x; };
	void SetRepeatOnAttack(int _x) { repeatonattack = _x; };
	void SetRepreatOnDraw(int _x) { repreatondraw = _x; };
	void SetRepeatOnExhaust(int _x) { repeatonexhaust = _x; };
	void SetRepeatOnExhaustAny(int _x) { repeatonexhaustany = _x; };
	void SetRepeatOnPay(int _x) { repeatonpay = _x; };
	void SetRepeatOnSac(int _x) { repeatonsac = _x; };
	void SetGenericResource(int _x) { genericresource = _x; };
	void SetResource(int _x) { resource = _x; };
	void SetCounterType(int _x) { countertype = _x; };
	void SetCounterAny(int _x) { counterany = _x; };
	void SetCounterAbility(int _x) { counterability = _x; };
	void SetCounterAnyCard(int _x) { counteranycard = _x; };
	void SetPreventAttack(int _x) { preventattack = _x; };
	void SetPreventBlock(int _x) { preventblock = _x; };
	void SetPreventBoth(int _x) { preventboth = _x; };
	void SetForceAttack(int _x) { forceattack = _x; };
	void SetForceBlock(int _x) { forceblock = _x; };
	void SetSwapStats(int _x) { swapstats = _x; };
	void SetDrawCard(int _x) { drawcard = _x; };
	void SetCreateToken(int _x) { createtoken = _x; };
	void SetDamageToAny(int _x) { damagetoany = _x; };
	void SetDamageToPlayer(int _x) { damagetoplayer = _x; };
	void SetDamageToUnit(int _x) { damagetounit = _x; };
	void SetFromBattlefield(int _x) { frombattlefield = _x; };
	void SetFromGrave(int _x) { fromgrave = _x; };
	void SetFromHand(int _x) { fromhand = _x; };
	void SetFromDeathzone(int _x) { fromdeathzone = _x; };
	void SetFromChronicle(int _x) { fromcronicle = _x; };
	void SetFromArchieve(int _x) { fromarchieve = _x; };
	void SetToBattlefield(int _x) { tobattlefield = _x; };
	void SetToGrave(int _x) { tograve = _x; };
	void SetToHand(int _x) { tohand = _x; };
	void SetToDeathzone(int _x) { todeathzone = _x; };
	void SetToChronicleTop(int _x) { tochronicletop = _x; };
	void SetToChronicle(int _x) { tochronicle = _x; };
	void SetTargetPerm(int _x) { targetperm = _x; };
	void SetTaregtResource(int _x) { taregtresource = _x; };
	void SetTargetUnit(int _x) { targetunit = _x; };
	void SetTargetEnhancement(int _x) { targetenhancement = _x; };
	void SetTargetYou(int _x) { targetyou= _x; };
	void SetTargetPlayer(int _x) { targetplayer = _x; };
	void SetTargetOpponent(int _x) { targetopponent = _x; };
	void SetTargetEveryone(int _x) { targeteveryone = _x; };
	void SetSacAny(int _x) { sacany = _x; };
	void SetSacType(int _x) { sactype = _x; };
	void SetAll(int _x) { all = _x; };
	void SetUEOT(int _x) { ueot = _x; };
	void SetReduceGeneric(int _x) { reducegeneric = _x; };
	void SetReduceResource(int _x) { reduceresource = _x; };
	void SetUnitsInYourGraveyard(int _x) { Unitsinyourgraveyard = _x; };
	// }

	// Getters for the local variables
	//{
	int GetUnit() { return unit; };
	int GetEnhancement() { return enhancement; };
	int GetConjured() { return conjured; };
	int GetTrick() { return trick; };
	int GetDeathzoneCost() { return deathzonecost; };
	int GetCommon() { return common; };
	int GetUncommon() { return uncommon; };
	int GetRare() { return rare; };
	int GetUnique() { return unique; };
	int GetFlying() { return flying; };
	int GetOverrun() { return overrun; };
	int GetResistance() { return resistance; };
	int GetBypass() { return bypass; };
	int GetInanimate() { return inanimate; };
	int GetDetermined() { return determined; };
	int GetRanged() { return ranged; };
	int GetFury() { return fury; };
	int GetHaste() { return haste; };
	int GetExtend() { return extend; };
	int GetBlocker() { return blocker; };
	int GetUnblockable() { return unblockable; };
	int GetAttack() { return attack; };
	int GetDefence() { return defence; };
	int GetBoonAttack() { return boonattack; };
	int GetBoonDefence() { return boondefence; };
	int GetTokenAttack() { return tokenattack; };
	int GetTokenDefence() { return tokendefence; };
	int GetBounce() { return bounce; };
	int GetSearch() { return search; };
	int GetRearrange() { return rearrange; };
	int GetLookAt() { return lookat; };
	int GetToReturn() { return toreturn; };
	int GetDestroy() { return destroy; };
	int GetReanimate() { return reanimate; };
	int GetDiscardChoice() { return discardchoice; };
	int GetDiscardRandom() { return discardrandom; };
	int GetTakeControl() { return takecontrol; };
	int GetPutIntoPlay() { return putintoplay; };
	int GetFreeze() { return freeze; };
	int GetExhaust () { return exhaust; };
	int GetPreventReady() { return preventready; };
	int GetStand() { return stand; };
	int GetRepeatOnAttack() { return repeatonattack; };
	int GetRepreatOnDraw() { return repreatondraw; };
	int GetRepeatOnExhaust() { return repeatonexhaust; };
	int GetRepeatOnExhaustAny() { return repeatonexhaustany; };
	int GetRepeatOnPay() { return repeatonpay; };
	int GetRepeatOnSac() { return repeatonsac; };
	int GetGenericResource() { return genericresource; };
	int GetResource() { return resource; };
	int GetCounterType() { return countertype; };
	int GetCounterAny() { return counterany; };
	int GetCounterAbility() { return counterability; };
	int GetCounterAnyCard() { return counteranycard; };
	int GetPreventAttack() { return preventattack; };
	int GetPreventBlock() { return preventblock; };
	int GetPreventBoth() { return preventboth; };
	int GetForceAttack() { return forceattack; };
	int GetForceBlock() { return forceblock; };
	int GetSwapStats() { return swapstats; };
	int GetDrawCard() { return drawcard; };
	int GetCreateToken() { return createtoken; };
	int GetDamageToAny() { return damagetoany; };
	int GetDamageToPlayer() { return damagetoplayer; };
	int GetDamageToUnit() { return damagetounit; };
	int GetFromBattlefield() { return frombattlefield; };
	int GetFromGrave() { return fromgrave; };
	int GetFromHand() { return fromhand; };
	int GetFromDeathzone() { return fromdeathzone; };
	int GetFromChronicle() { return fromcronicle; };
	int GetFromArchieve() { return fromarchieve; };
	int GetToBattlefield() { return tobattlefield; };
	int GetToGrave() { return tograve; };
	int GetToHand() { return tohand; };
	int GetToDeathzone() { return todeathzone; };
	int GetToChronicleTop() { return tochronicletop; };
	int GetToChronicle() { return tochronicle; };
	int GetTargetPerm() { return targetperm; };
	int GetTargetResource() { return taregtresource; };
	int GetTargetUnit() { return targetunit; };
	int GetTargetEnhancement() { return targetenhancement; };
	int GetTargetYou() { return targetyou; };
	int GetTargetPlayer() { return targetplayer; };
	int GetTargetOpponent() { return targetopponent; };
	int GetTargetEveryone() { return targeteveryone; };
	int GetSacAny() { return sacany; };
	int GetSacType() { return sactype; };
	int GetAll() { return all; };
	int GetUEOT() { return ueot; };
	int GetReduceGeneric() { return reducegeneric; };
	int GetReduceResource() { return reduceresource; };
	int GetUnitsInYourGraveyard() { return Unitsinyourgraveyard; };
	// }

	// Functions for checking the cost of values that are passed as strings.
	// {
	int CheckKeyword(string _value);
	int CheckLocation(string _value, string _direction);
	// }

	// Function to load the card costs from a CSV
	void LoadCosts();
};
