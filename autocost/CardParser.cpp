#include "CardParser.h"
#include "CardCosts.h"

CardParser::CardParser()
{
	Card* cards = new Card[1];
	string filename = "";
}

CardParser::~CardParser()
{

}

void CardParser::ParseCSV(string _filename)
{
	filename = _filename;

	csv.OpenFile(filename);

	csv.ReadFile();

	csv.SplitLines();

	csv.SplitCols();

	csv.CloseFile();
}

char CardParser::ResourceType(string _resource)
{
	if (_resource[0] == 'A')
	{
		return 'A';
	}
	else if (_resource[0] == 'E')
	{
		return 'E';
	}
	else if (_resource[0] == 'F')
	{
		return 'F';
	}
	else if (_resource[0] == 'M')
	{
		return 'M';
	}
	else if (_resource[0] == 'W' && _resource[1] == 'a')
	{
		return 'T';
	}
	else if (_resource[0] == 'W' && _resource[1] == 'o')
	{
		return 'W';
	}
	
	return 'X';
}

void CardParser::InitCardList()
{
	cardcount = csv.GetCardCount();
	cards = new Card[cardcount + 1];
	colcount = csv.GetColCount();
}

void CardParser::BuildCards()
{
	char res = ' ';
	string resource = "";

	for (int i = 1; i < cardcount + 1; i++)
	{
		res = ' ';
		cards[i - 1].SetName(csv.GetCol(i, 0));
		cards[i - 1].SetType(csv.GetCol(i, 2));
		cards[i - 1].SetSubType(csv.GetCol(i, 3));
		cards[i - 1].SetRarity(csv.GetCol(i, 4));
		cards[i - 1].SetAttack(csv.GetCol(i, 5));
		cards[i - 1].SetDefence(csv.GetCol(i, 6));
		cards[i - 1].SetResource(csv.GetCol(i, 7));
		resource = cards[i - 1].GetResource();
		res = ResourceType(resource);
		cards[i - 1].SetResourceSymbol(res);
		cards[i - 1].SetAbility(csv.GetCol(i, 8));
		cards[i - 1].SetPlainAbility(csv.GetCol(i, 9));
		cards[i - 1].SetDeathzoneAbility(csv.GetCol(i, 10));
		cards[i - 1].SetPlainDeathzoneAbility(csv.GetCol(i, 11));
		cards[i - 1].SetFlavourText(csv.GetCol(i, 12));
	}
}

void CardParser::CostCards()
{
	CostToEffect currentcost;

	string tempability = "";
	string lowerability = "";
	string tempkeyword;
	int abilities = 0;
	bool firstpass = false;

	for (int c = 0; c < cardcount; c++)
	{
		cards[c].SetTotalCost(0);

		if (cards[c].GetType() == "Unit")
		{
			int attack = 0, defence = 0, cost = 0;

			if (cards[c].GetAttack() == "*")
			{
				attack = costs.GetUnitsInYourGraveyard();
			}
			else
			{
				attack = stoi(cards[c].GetAttack());
			}

			if (cards[c].GetDefence() == "*")
			{
				defence = costs.GetUnitsInYourGraveyard();
			}
			else
			{
				defence = stoi(cards[c].GetDefence());
			}

			cost = (attack * costs.GetAttack()) + (defence * costs.GetDefence());

			cards[c].AddToTotalCost(cost);
		}
		else if (cards[c].GetType() == "Enhancement")
		{
			cards[c].AddToTotalCost(costs.GetEnhancement());
		}
		else if (cards[c].GetType() == "Conjured")
		{
			cards[c].AddToTotalCost(costs.GetConjured());
		}
		else if (cards[c].GetType() == "Trick")
		{
			cards[c].AddToTotalCost(costs.GetTrick());
		}
		else
		{

		}

		if (cards[c].GetRarity() == "Common")
		{
			cards[c].SubtractTotalCost(costs.GetCommon());
		}
		else if (cards[c].GetRarity() == "Uncommon")
		{
			cards[c].SubtractTotalCost(costs.GetUncommon());
		}
		else if (cards[c].GetRarity() == "Rare")
		{
			cards[c].SubtractTotalCost(costs.GetRare());
		}
		else if (cards[c].GetRarity() == "Unique")
		{
			cards[c].SubtractTotalCost(costs.GetUnique());
		}
		
		// Handle the card ability
		// {
		lowerability = cards[c].GetAbility();
		cards[c].SetAbility("");
		tempability = "";

		// Change the ability to all lower case letters
		for (int i = 0; i < lowerability.length(); i++)
		{
			tempability += tolower(lowerability[i]);
		}

		if (tempability.find('\\') == 0)
		{
			string keyword = "";
			int times = 0;
			int cost = 0;
			int length = tempability.length();

			for (int i = 1; i < length; i++)
			{
				if (tempability[i] == '\\')
				{
					cost = costs.CheckKeyword(keyword);
					if (cost != -1)
					{
						cards[c].AddCost(cost);
						cards[c].HandleKeyWord(cards[c].GetName(), keyword, times);
						tempability.erase(0, keyword.length() + 1);
						//cards[c].SetAbility(tempability);
						keyword = "";
						times++;
						i = 0;
						length = tempability.length();
					}
					else
					{
						currentcost = CostToEffect::cardcost;

						cards[c].SetCurrentCost(currentcost);

						firstpass = true;

						CostSection(c, keyword, firstpass);

						tempability.erase(0, keyword.length());
						i = 1 + abilities;;
						abilities++;
						keyword = "";
						length = tempability.length();
					}
				}
				else
				{
					keyword += tempability[i];
				}
			}

			cost = costs.CheckKeyword(keyword);
			if (cost != -1)
			{
				cards[c].AddCost(cost);
				cards[c].HandleKeyWord(cards[c].GetName(), keyword, times);
				tempability.erase(0, keyword.length() + 1);
				cards[c].SetAbility(tempability);
			}
			else
			{
				currentcost = CostToEffect::cardcost;

				cards[c].SetCurrentCost(currentcost);

				firstpass = true;

				CostSection(c, keyword, firstpass);

				tempability.erase(0, keyword.length() + 1);
			}
		}
		// }

		// Handle deathzone ability
		// {
		lowerability = cards[c].GetDeathzoneAbility();
		cards[c].SetDeathzoneAbility("");
		tempability = "";

		// Change the ability to all lower case letters
		for (int i = 0; i < lowerability.length(); i++)
		{
			tempability += tolower(lowerability[i]);
		}

		// Change the deathzone ability to all lower case letters
		if (tempability.find('\\') == 0)
		{
			string keyword = "";
			int times = 0;
			int cost = 0;
			int length = tempability.length();

			for (int i = 1; i < length; i++)
			{
				if (tempability[i] == '\\')
				{
					cost = costs.CheckKeyword(keyword);
					if (cost != -1)
					{
						cards[c].AddCost(cost);
						cards[c].HandleKeyWord(cards[c].GetName(), keyword, times);
						tempability.erase(0, keyword.length() + 1);
						keyword = "";
						times++;
						i = 0;
						length = tempability.length();
					}
					else
					{
						currentcost = CostToEffect::deathzonecost;

						cards[c].SetCurrentCost(currentcost);

						firstpass = true;

						CostSection(c, keyword, firstpass);

						tempability.erase(0, keyword.length() + 1);
						i = 0;
						keyword = "";
						length = tempability.length();
					}
				}
				else
				{
					keyword += tempability[i];
				}
			}

			cost = costs.CheckKeyword(keyword);
			if (cost != -1)
			{
				cards[c].AddCost(cost);
				cards[c].HandleKeyWord(cards[c].GetName(), keyword, times);
				tempability.erase(0, keyword.length() + 1);
			}
			else
			{
				currentcost = CostToEffect::deathzonecost;

				cards[c].SetCurrentCost(currentcost);

				firstpass = true;

				CostSection(c, keyword, firstpass);

				tempability.erase(0, keyword.length() + 1);
			}
		}
		// }

		abilities = 0;
	}
}

void CardParser::CostSection(int _c, string _tempability, bool _firstpass)
{
	int foundoptarg = 0;
	int foundmandarg = 0;
	int foundcondarg = 0;
	int foundfloating = 0;
	int argend = 0;
	string keyword = "";
	string arglist = "";
	bool onearg = false;

	while (_tempability != "\0")
	{
		foundoptarg = _tempability.find('[');
		foundmandarg = _tempability.find('{');
		foundcondarg = _tempability.find('<');
		foundfloating = _tempability.find("(");

		if (foundoptarg == -1 || foundmandarg == -1)
		{
			onearg = true;
		}

		if (foundfloating == 0)
		{
			argend = _tempability.find(')');

			arglist += CutPart(_tempability, argend);

			arglist += '#';

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}

			_firstpass = false;
		}
		else if (foundcondarg == 0)
		{
			argend = _tempability.find('>');

			arglist += CutPart(_tempability, argend);

			arglist += '~';

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}
		}
		else if (foundoptarg < foundmandarg && onearg != true)
		{
			argend = _tempability.find(']');

			arglist += ',';

			arglist += CutPart(_tempability, argend);

			arglist += '.';

			foundcondarg = _tempability.find('<');

			if (foundcondarg == 0)
			{
				if (foundcondarg != -1)
				{
					argend = _tempability.find('>');

					arglist += CutPart(_tempability, argend);

					arglist += '~';
				}
			}

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}

			_firstpass = false;
		}
		else if (foundmandarg < foundoptarg && onearg != true)
		{
			if (_firstpass == true)
			{
				argend = _tempability.find('{');
				// Has a mandatory arg first.
				for (int i = 0; i < argend; i++)
				{
					keyword += _tempability[i];
				}

				_tempability.erase(0, argend);
			}

			argend = _tempability.find('}');

			arglist += CutPart(_tempability, argend);

			arglist += '|';

			foundcondarg = _tempability.find('<');

			if (foundcondarg == 0)
			{
				if (foundcondarg != -1)
				{
					//arglist += '~';
					argend = _tempability.find('>');

					arglist += CutPart(_tempability, argend);

					arglist += '~';
				}
			}

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}

			_firstpass = false;
		}
		else if (foundmandarg != 0 && foundoptarg == -1)
		{
			
			if (_firstpass == true)
			{
				// Has a madatory arg and no optional arg
				argend = _tempability.find('{');
				// Has a mandatory arg first.
				for (int i = 0; i < argend; i++)
				{
					keyword += _tempability[i];
				}

				_tempability.erase(0, argend);
			}

			argend = _tempability.find('}');

			arglist += CutPart(_tempability, argend);

			arglist += '|';

			foundcondarg = _tempability.find('<');

			if (foundcondarg == 0)
			{
				if (foundcondarg != -1)
				{
					argend = _tempability.find('>');

					arglist += CutPart(_tempability, argend);

					arglist += '~';
				}
			}

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}

			_firstpass = false;
		}
		else if (foundoptarg == 0 && foundmandarg == -1)
		{
			argend = _tempability.find(']');

			arglist += ',';

			arglist += CutPart(_tempability, argend);

			arglist += '.';

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}

			_firstpass = false;
		}
		else if (foundmandarg == 0 && foundoptarg == -1)
		{
			argend = _tempability.find('}');

			arglist += CutPart(_tempability, argend);

			arglist += '|';

			if (_tempability.length() == 0)
			{
				cards[_c].HandleKeyWord(cards[_c].GetName(), keyword, arglist);
			}

			_firstpass = false;
		}		
	}
}

// Function to divide up the current section
string CardParser::CutPart(string& _section, int _point)
{
	string arg = "";

	for (int i = 1; i < _point; i++)
	{
		arg += _section[i];
	}

	_section.erase(0, _point + 1);

	return arg;
}

void CardParser::GenerateCosts()
{
	for (int c = 0; c < GetCardCount(); c++)
	{
		cards[c].SetCost(DefineCost(cards[c].GetTotalCost(), cards[c].GetResourceSymbol()));
		cards[c].SetAbilityCost(DefineAbilityCost(cards[c].GetAbilityTotalCost(), cards[c].GetResourceSymbol()));
		cards[c].SetDeathzoneCost(DefineDeathzoneCost(cards[c].GetDeathzoneTotalCost()));

		cards[c].ConvertCost();
	}
}

string CardParser::DefineCost(int _cost, char _res)
{
	int totalcost = 0;
	int elementtoadd = 0;
	int generictoadd = 0;
	char res = ' ';
	string thecost = "";

	totalcost = _cost;
	res = _res;

	if (totalcost == 0)
	{
		return thecost;
	}

	if (totalcost <= 80)
	{
		elementtoadd++;
	}

	while (totalcost >= 80)
	{
		if (elementtoadd == 0)
		{
			totalcost -= 100;
			elementtoadd++;
		}
		else if (totalcost % 100 == 0)
		{
			totalcost -= 100;
			elementtoadd++;
		}
		else
		{
			totalcost -= 80;
			generictoadd++;
		}
	}

	if (generictoadd == 0)
	{

	}
	else
	{
		thecost = to_string(generictoadd);
	}

	for (int i = 0; i < elementtoadd; i++)
	{
		thecost += res;
	}

	return thecost;
}

string CardParser::DefineAbilityCost(int _cost, char _res)
{
	int totalcost = 0;
	int elementtoadd = 0;
	int generictoadd = 0;
	char res = ' ';
	string thecost = "";

	totalcost = _cost;
	res = _res;

	if (totalcost == 0)
	{
		return thecost;
	}

	while (totalcost >= 80)
	{
		if (totalcost % 100 == 0)
		{
			totalcost -= 100;
			elementtoadd++;
		}
		else
		{
			totalcost -= 80;
			generictoadd++;
		}
	}

	if (generictoadd == 0)
	{

	}
	else
	{
		thecost = to_string(generictoadd);
	}

	for (int i = 0; i < elementtoadd; i++)
	{
		thecost += res;
	}

	return thecost;
}

string CardParser::DefineDeathzoneCost(int _cost)
{
	int totalcost = 0;
	int generictoadd = 0;
	string thecost = "";

	totalcost = _cost;

	if (totalcost == 0)
	{
		return thecost;
	}

	while (totalcost > 80)
	{
		totalcost -= 80;
		generictoadd++;
	}

	if (generictoadd == 0)
	{

	}
	else
	{
		thecost = to_string(generictoadd);
	}

	return thecost;
}

string CardParser::GetCard(int num)
{
	string card;
	string newcost = "";
	string newabilitycost = "";
	string newdeathzonecost = "";
	string tempcost = "";
	string tempabilitycost = "";
	string tempdeathzonecost = "";
	string gen = "";
	char tempchar = ' ';

	tempcost = cards[num].GetCost();
	tempabilitycost = cards[num].GetAbilityCost();
	tempdeathzonecost = cards[num].GetDeathzoneCost();

	for (int i = 0; i < tempcost.length(); i++)
	{
		tempchar = tempcost[i];

		if (tempchar == 'A' || tempchar == 'E' || tempchar == 'F' || tempchar == 'M' || tempchar == 'T' || tempchar == 'W')
		{
			newcost += "\\";
			newcost += tempcost[i];
		}
		else
		{
			newcost += tempcost[i];
		}
	}

	tempchar = ' ';

	for (int i = 0; i < tempabilitycost.length(); i++)
	{
		tempchar = tempabilitycost[i];

		if (tempchar == 'A' || tempchar == 'E' || tempchar == 'F' || tempchar == 'M' || tempchar == 'T' || tempchar == 'W')
		{
			newabilitycost += "\\";
			newabilitycost += tempabilitycost[i];
		}
		else
		{
			newabilitycost += tempabilitycost[i];
		}
	}

	for (int i = 0; i < tempdeathzonecost.length(); i++)
	{
		tempchar = tempdeathzonecost[i];

		if (tempchar == 'A' || tempchar == 'E' || tempchar == 'F' || tempchar == 'M' || tempchar == 'T' || tempchar == 'W')
		{
			newdeathzonecost += "\\";
			newdeathzonecost += tempdeathzonecost[i];
		}
		else
		{
			newdeathzonecost += tempdeathzonecost[i];
		}
	}

	if (cards[num].GetGeneric() == "")
	{
		gen = "0";
	}
	else
	{
		gen = cards[num].GetGeneric();
	}

	card = cards[num].GetName();
	card += "|";
	card += newcost;
	card += "|";
	card += cards[num].GetX();
	card += "|";
	card += gen;
	card += "|";
	card += cards[num].GetAir();
	card += "|";
	card += cards[num].GetEarth();
	card += "|";
	card += cards[num].GetFire();
	card += "|";
	card += cards[num].GetMetal();
	card += "|";
	card += cards[num].GetWater();
	card += "|";
	card += cards[num].GetWood();
	card += "|";
	card += "|";
	card += cards[num].GetType();
	card += "|";
	card += cards[num].GetSubType();
	card += "|";
	card += cards[num].GetRarity();
	card += "|";
	card += cards[num].GetAttack();
	card += "|";
	card += cards[num].GetDefence();
	card += "|";
	card += cards[num].GetResource();
	card += "|";
	card += cards[num].GetKeywords();
	card += "|";
	card += newabilitycost;
	card += "|";
	card += cards[num].GetAbility();
	card += "|";
	card += newdeathzonecost;
	card += "|";
	card += cards[num].GetDeathzoneAbility();
	card += "|";
	card += cards[num].GetFlavourText();

	cout << card;

	return card;
}

void CardParser::PrintCard(int cardno)
{
	cards[cardno].PrintCard();
}