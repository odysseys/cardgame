#pragma once

#include "CSVParser.h"
#include "CardCosts.h"
#include "Card.h"
#include "Enums.h"

class CardParser
{
/*public:
	static CardParser& Instance()
	{
		static CardParser cardparser;
		return singleton;
	}*/
private:
	CSVParser csv;
	CardCosts costs;
	Card* cards;
	string filename;

	int cardcount = 0;
	int colcount = 0;

	// Function for defining the cost as resources
	string DefineCost(int _cost, char _res);
	string DefineAbilityCost(int _cost, char _res);
	string DefineDeathzoneCost(int _cost);

public:
	CardParser();
	~CardParser();

	CardParser(Card&&) {};

	CardParser(const Card&) = delete;
	CardParser& operator=(const Card&) = delete;

	/*CardParser(const CardParser&);
	CardParser operator=(const CardParser&);*/

	// Function for getting the amount of cards
	int GetCardCount() { return cardcount; }
	string GetCard(int num);

	// Function for handling the CSV parsing
	void ParseCSV(string filename);

	char ResourceType(string _resource);

	// Function for setting up the card list to empty
	void InitCardList();
	// Function for building the card list from the file
	void BuildCards();
	// Function to cost the cards
	void CostCards();
	// Function to cost the current section
	void CostSection(int _c, string _tempability, bool _firstpass);
	// Function to divide up the current section
	string CutPart(string& _section, int _point);
	// Function to generate the card costs
	void GenerateCosts();
	// Function for printing a basic layout of a specific card
	void PrintCard(int cardno);
};

