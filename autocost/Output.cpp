#include "Output.h"

Output::Output()
{

}

Output::~Output()
{
    csv.close();
}

// Functions for handling the file
// {
void Output::OpenFile(string _filename)
{
    filename = _filename;
    if (filename == "")
    {
        cout << "No filename" << endl;
    }
    else
    {
        csv.open(filename, ios::out | ios::app);

        if (!csv)
        {
            cout << "File not opened!" << endl;
        }
        else
        {
            cout << "File opened successfully!" << endl;
        }
    }
}

void Output::SaveFile()
{
    cout << file;

    for (int i = 0; i < file.length(); i++)
    {
        csv << file[i];
    }
}

void Output::CloseFile()
{
    csv.close();
}
// }

// Functions for creating the file
// {
void Output::CreateTopLine()
{
    line = "Name|Cost|x|Generic|Air|Earth|Fire|Metal|Water|Wood|Image|Type|Sub Type|Rarity|Attack|Defence|Resource|Keywords|Ability Cost|Ability|Death Zone Cost|Death Zone Ability|Flavour Text";

    file = line;
    file += nl;
}

void Output::CreateLine(string _line)
{
    string line = "";

    file += _line;
    file += nl;
}

void Output::CreateFile(int _cardcount)
{
    int cardcount = 0;

    cardcount = _cardcount;

    CreateTopLine();
}
// }