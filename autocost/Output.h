#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "Card.h"
#include "Enums.h"

using namespace std;

class Output
{
private:
	string filename;
	string file;
	string line;
	ofstream csv;
	char nl = '\n';
	int pos = -1;
public:
	Output();
	~Output();

	// Functions for handling the file
	// {
	void OpenFile(string _filename);
	void SaveFile();
	void CloseFile();
	// }

	// Functions for creating the file
	// {
	void CreateTopLine();
	void CreateLine(string _line);
	void CreateFile(int _cardcount);
	// }
};
