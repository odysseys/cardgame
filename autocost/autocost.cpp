// Autocost.cpp : Defines the entry point for the application.
//
#include "autocost.h"

using namespace std;

void OutputCards(CardParser* _card, string _filename, int _count)
{
	Output output;

	output.OpenFile(_filename);
	output.CreateFile(_count);
	for (int c = 0; c < _count; c++)
	{
		output.CreateLine(_card->GetCard(c));
	}
	output.SaveFile();
	output.CloseFile();
}

string OutputCheck()
{
	string filename = "";
	string custom = "";

	cout << "Do you wish to use a custom output CSV file, Y or N" << endl;
	cin >> custom;

	if (custom == "y" || custom == "Y")
	{
		cout << "Please type cost file path and output filename" << endl;
		cin >> filename;
	}
	else
	{
#ifdef MAKEFILE
		filename = "../output.csv";
#else
		filename = "../../../../output.csv";
#endif
	}

	return filename;
}

int main()
{
	CardParser card;
	CardCosts cardcosts;
	Card tempcard;

#ifdef MAKEFILE
	string start = "../";
#else
	string start = "../../../../";
#endif
	string extension = ".csv";
	string elements[6] = { "Air", "Fire", "Earth", "Metal", "Water", "Wood" };
	int elementno = 6;

	char cust = ' ';
	string custom = "";
	string temp = "";
	string filename = "";
	int cardcount = 0;

#ifdef TEST
	filename = "../Test.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef AIR
	filename = "../Air.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef EARTH
	filename = "../Earth.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef FIRE
	filename = "../Fire.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef METAL
	filename = "../Metal.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef WATER
	filename = "../Water.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef WOOD
	filename = "../Wood.csv";

	card.ParseCSV(filename);
	card.InitCardList();
	card.BuildCards();
	card.CostCards();
	card.GenerateCosts();
	for (int c = 0; c < card.GetCardCount(); c++)
	{
		card.PrintCard(c);
	}

	filename = "../output.csv";

	OutputCards(&card, filename, card.GetCardCount());
#endif

#ifdef ALL
	for (int i = 0; i < elementno; i++)
	{
		filename = start;
		filename += elements[i];
		filename += extension;

		card.ParseCSV(filename);
		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = "../output.csv";

		OutputCards(&card, filename, card.GetCardCount());
	}
#else
	cout << "Starting...\n";
	cout << "Do you wish to use a custom cost CSV file, Y or N" << endl;
	cin >> cust;

	if (cust == 'y' || cust == 'Y')
	{
		cout << "Please type cost file path and cost filename" << endl;
		cin >> filename;
	}

	cout << "Select a file(s) you wish to process." << endl;
	cout << "All the cards: all" << endl;
	cout << "An element: air, earth, fire, metal, water or wood" << endl;
	cout << "The test CSV: test" << endl;
	cout << "Or do you wish to use a custom CSV file: y" << endl;
	cin >> custom;

	temp = custom;
	custom = "";

	// Change the ability to all lower case letters
	for (int i = 0; i < temp.length(); i++)
	{
		custom += tolower(temp[i]);
	}

	if (custom == "all")
	{

		for (int i = 0; i < elementno; i++)
		{
			filename = start;
			filename += elements[i];
			filename += extension;

			card.ParseCSV(filename);
			card.InitCardList();
			card.BuildCards();
			card.CostCards();
			card.GenerateCosts();
			for (int c = 0; c < card.GetCardCount(); c++)
			{
				card.PrintCard(c);
			}

#ifdef MAKEFILE
			filename = "../output.csv";
#else
			filename = "../../../../output.csv";
#endif

			OutputCards(&card, filename, card.GetCardCount());
		}
/*
#ifdef MAKEFILE
		filename = "../Air.csv";
#else
		filename = "../../../../Air.csv";
#endif

		card.ParseCSV(filename);

		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());

#ifdef MAKEFILE
		filename = "../Earth.csv";
#else
		filename = "../../../../Earth.csv";
#endif

		card.ParseCSV(filename);

		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());

#ifdef MAKEFILE
		filename = "../Fire.csv";
#else
		filename = "../../../../Fire.csv";
#endif

		card.ParseCSV(filename);

		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());

#ifdef MAKEFILE
		filename = "../Metal.csv";
#else
		filename = "../../../../Metal.csv";
#endif

		card.ParseCSV(filename);

		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());

#ifdef MAKEFILE
		filename = "../Water.csv";
#else
		filename = "../../../../Water.csv";
#endif

		card.ParseCSV(filename);

		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());

#ifdef MAKEFILE
		filename = "../Wood.csv";
#else
		filename = "../../../../Wood.csv";
#endif

		card.ParseCSV(filename);

		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());
		*/
	}
	else if (custom == "air" || custom == "earth" || custom == "fire" || custom == "metal" || custom == "water" || custom == "wood")
	{

		filename = custom + ".csv";

#ifdef MAKEFILE
	filename = "../" + filename;
#else
	filename = "../../../../" + filename;
#endif

		card.ParseCSV(filename);
		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		 filename = OutputCheck();

		 OutputCards(&card, filename, card.GetCardCount());
	}
	else if (custom == "test")
	{
#ifdef MAKEFILE
	filename = "../Test.csv";
#else
	filename = "../../../../Test.csv";
#endif

		card.ParseCSV(filename);
		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());
	}
	else if (custom == "y")
	{
		cout << "Please type file path and filename" << endl;
		cin >> filename;

		card.ParseCSV(filename);
		card.InitCardList();
		card.BuildCards();
		card.CostCards();
		card.GenerateCosts();
		for (int c = 0; c < card.GetCardCount(); c++)
		{
			card.PrintCard(c);
		}

		filename = OutputCheck();

		OutputCards(&card, filename, card.GetCardCount());
	}
#endif

	return 0;
}
