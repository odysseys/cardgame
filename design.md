﻿# Elements


- Fire
- Water
- Wind
- Earth
- Wood
- Metal


# Zones


- Battlefield
- Resource
- Deck
- Graveyard
- Death (tracked on players side for that players life)


# Deck construction


Players will bring a 130 card archive with them.
Players with then in turns choose from the 6 element types to create their decks with.
Decks can only contain cards with colour identity of a colour the player picked.
Players will then have a set time frame to make their decks from their libraries and begin play.
Any deck can only contain 3 copies of any cards with the same name.


# Game phases


- Ready
- Draw
- Main phase
- End


## Special phases

## Combat

# Special Actions

## Play a resource card

# Game Phase Breakdown

## Ready

The active player untaps all permanent cards they own. No other players can act during this phase.

## Draw

The active player draws one card from their deck. No other players can act during this phase.

## Main phase

During the main phase the active can play any cards they can afford to pay for with resources they own, this can be reacted to by other players . They can perform a special action of playing a resource action, this action cannot be reacted to by any other player. They can attack with any creatures they own that do not have summoning sickness.


## Combat phase
As a unit is declared as an attacker, the active player moves into the special combat phase, other players can react to this change in phase. The active player declares a target for the unit to attack, other players can react to this declaration. Damage is then dealt to the target, if the target has an attack value it deals that damage back to the attacking unit. If a units defense is less than the damage it has reviewed it does and it moved to the graveyard. If the damage is dealt to a player, that player puts a card from the top of his/her deck into his/her death zone for each point. The attack phase does not cause resource to empty from any players resource stock.

## End Phase

Any until end of turn effects will end here. The other players get a chance to react to this phase.

## Cleanup

All damage on any unit is removed. No players can act during this phase.

# Card types

- Unit
- Enhancement
- Trick
- Conducted
- Alchemist?

# Card Types Explained

## Unit

Units are played into the battle zone once the resource cost is paid, players can react to the card being played.
Once a unit enters the battle zone, any enter the battle zone effects will trigger.
Readied unit cards act as blockers for the player, if a readied unit in the battle zone during the declare target step then that player cannot be targeted.
If a unit is declared as an attacked it is fatigued.


## Enhancement

Enhancement cards will improve the effectiveness of something in the game.
Enhancement sub types are unit enhancement, global enhancement, resource enhancement and death enhancement.
Other than global enhancements all enhancements need a target of their type to enhance.
If a card enhanced with an enhancement ever goes to the graveyard, all enhancements on it go to the graveyard as well.


## Trick

A trick card is a card that can be played at any time.
They can have power effects that can change the decision being made when played to react to another action.


## Conjured

A conjured card is a card that can only be played if you are the active player and the stack is empty.
They provide on time effects, or effects with a limited time effect, i.e. until end of turn.


# Rarity

- Common
- Uncommon
- Rare
- Unique

# Resource

All cards will have a resource generating ability.
Some cards may have special resource generating abilities or be able to produce other elements resource, this ability does not count towards colour identity.
Players can play one card into the resource zone per turn.
Cards in the resource zone are tapped to add whatever resources they add to your resource stock.


## Resource Stock

A players resource stock is where they store any resource they have added by tapping a resource card during the current phase.
It can be spent to play cards or activate abilities on cards requiring a resource cost.
The resource stock is emptied at the end of the end step.


# Death Zone Abilities

Some cards will have an ability that is triggered when it enters the death zone.
These cards are applied to the player who owns them. 


## Death Zone Card Play

Death zone is an ordered zone.
Cards in the death zone have a cost which is more than its regular casting cost.
Only players who do not own the card can pay the death zone cost to play the card.
Once played it will move to its owner's graveyard.

# Win Conditions

Reach 15 cards in your opponents death zone.
Damage dealt to a player puts the top card of that players deck into their death zone.

# Lose condition

If a player ever has to draw a card and cannot that player loses the game.

# Allied Elements

- Air/Fire
- Water/Wood
- Earth/Metal

# Keyword Ideas

## Overrun

Any damage above the defence of a unit spills over to the player.

## Bypass

The unit can ignore the defence of any blocking unit and apply damage directly to the player.
The unit will still receive damage from any blocking unit.

## Resistance <element> <amount>

The unit will only take half damage from an elemental type

## Inanimate

This unit cannot function on it's own, when it enters or another unit enters they can operate this unit. While a unit is operating another unit, it cannot be the target of any card and cannot block.

## Determined 
A unit with this ability that goes to the graveyard from the battle zone and did not have a +1 +1 counter on it, will return to the battle zone at the beginning of the next end step with a +1/+1 counter.

## Ranged

Ranged units can block flying units and can only be blocked by ranged units or flying units.

## Fury

An Unit can attack again during combat.

## Haste

A unit with haste does not have summoning sickness, so it can attack or tap the turn it enters play.

## Extend <cost>

Change target to all.

## Blocker

This unit cannot attack.

## Unbllockable <element>

Cannot be block by either anything or a specific element.

